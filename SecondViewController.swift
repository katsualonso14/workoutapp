
import UIKit

class SecondViewController: UIViewController,UITableViewDelegate, UITableViewDataSource{
    
    var GetPicker:String!
    var GetDatePicker:String!
    
    var memoArray = [String]()
    let ud = UserDefaults.standard
    
    @IBOutlet weak var memoTableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return memoArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "memoCell", for: indexPath)
        cell.textLabel?.text = memoArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "toDetail", sender: nil)
        //押したら押した状態を解除
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    @IBAction func decisionButton(_ sender: Any) {
    }
    
    @IBAction func editButton(_ sender: Any) {
        let storyboard: UIStoryboard = self.storyboard!
        let first = storyboard.instantiateViewController(withIdentifier: "first")
        
        self.present(first, animated: true, completion: nil)
    }
    
    @IBAction func calendarButton(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        memoTableView.delegate = self
        memoTableView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        loadMemo()
    }
    
    func loadMemo(){
        if ud.array(forKey: "memoArray") != nil{
            //取得 またas!でアンラップしているのでnilじゃない時のみ
            memoArray = ud.array(forKey: "memoArray") as![String]
            //reloadしてくれる
            memoTableView.reloadData()
        }
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            //resultArray内のindexPathのrow番目をremove（消去）する
            memoArray.remove(at: indexPath.row)
            
            //再びアプリ内に消去した配列を保存
            ud.set(memoArray, forKey: "memoArray")
            
            //tableViewを更新
            tableView.reloadData()
        }
    }
}
